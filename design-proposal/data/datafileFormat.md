# Data File

## People

```
id, name

Example:
0, Alice
1, Bob
2, Jack
3, Lily
```

## People Properties

```
id, propertyKey, propertyValue
0, name, Alice
1, name, Bob
2, name, Jack
3, name, Lily
0, gender, female
0, age, 23
1, gender, male
1, age, 25
```

## Relationships

```
relationshipId

0
1
2
```

## Relationship Properties

```
id, propertyKey, propertyValue
0, from, 0
0, to, 1
0, description, Alice is the wife of Bob
1, from, 2
1, to, 3
1, description, Jack is the husband of Lily
2, from, 1
2, to, 3
2, description -> Bob is the father of Lily
```
