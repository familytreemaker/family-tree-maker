import * as React from "react";
import ReactDOM from "react-dom";
import AppLayout from "./AppLayout";

ReactDOM.render(
  <React.StrictMode>
    <AppLayout />
  </React.StrictMode>,
  document.getElementById("root")
);
