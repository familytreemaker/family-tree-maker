import { getAuth } from "@firebase/auth";

async function protectedFetch(context, url, requestBody) {
  const firebaseUser = getFirebaseUser(context);
  const idToken = firebaseUser && (await firebaseUser.getIdToken());
  const body = requestBody || {};
  const headers = body["headers"] || {};
  headers["Authorization"] = `Bearer ${idToken}`;
  headers["Content-Type"] = "application/json";
  body["headers"] = headers;
  const response = await fetch(url, body);
  return response.json();
}

export function getFirebaseUser(context) {
  const auth = getAuth(context);
  const user = auth.currentUser;
  return user;
}

export async function getPersonInfo(id, context) {
  const personInfo = await protectedFetch(context, `/api/get/${id}`);
  return personInfo;
}

export async function getData(context) {
  const familyTreeData = await protectedFetch(context, `/api/getAll`);
  return familyTreeData;
}

export async function createNode(node, context) {
  const createdNode = await protectedFetch(context, `/api/create`, {
    method: "post",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(node),
  });
  return createdNode;
}

export async function saveData(context) {
  return await protectedFetch(context, `/api/save`, {
    method: "post",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
  });
}

export async function createRelationship(relationshipObject, context) {
  return await protectedFetch(context, `/api/create-relationship`, {
    method: "post",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(relationshipObject),
  });
}

export function getChildrenIds(data, parentId) {
  if (parentId in data && "children" in data[parentId]) {
    return data[parentId]["children"];
  } else {
    return [];
  }
}

export function getName(data, id) {
  if (id in data && "name" in data[id]) {
    return data[id]["name"];
  }
}

export function getMembers(data) {
  return Object.keys(data)
    .filter((key) => data[key]["name"] !== "")
    .map((key) => ({
      id: key,
      label: data[key]["name"],
    }));
}

export function getRelationships(data, id) {
  let relationships = [];
  let relationshipKeys = ["father", "mother", "husband", "wife", "spouse"];
  if (id in data) {
    let node = data[id];
    if ("parents" in node) {
      for (let child of node["parents"]) {
        let relationshipID = child;
        let name = getName(data, relationshipID);
        if (name === undefined || name === "") {
          continue;
        }
        relationships.push({
          id: relationshipID,
          key: "parent",
          value: name,
        });
      }
    }
    for (let relationshipKey of relationshipKeys) {
      if (relationshipKey in node) {
        let relationshipID = node[relationshipKey];
        let name = getName(data, relationshipID);
        if (name === undefined || name === "") {
          continue;
        }
        relationships.push({
          id: relationshipID,
          key: relationshipKey,
          value: name,
        });
      }
    }
    if ("children" in node) {
      for (let child of node["children"]) {
        let relationshipID = child;
        let name = getName(data, relationshipID);
        if (name === undefined || name === "") {
          continue;
        }
        relationships.push({
          id: relationshipID,
          key: "child",
          value: name,
        });
      }
    }
    return relationships;
  }
}

export function getRandomKeyValue(data) {
  if (Object.keys(data).length > 0) {
    let key = Object.keys(data)[0];
    let value = data[key]["name"];
    return [key, value];
  } else {
    let key = "";
    let value = "";
    return [key, value];
  }
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}

export function createMember(data, name) {
  let id = uuidv4();
  while (id in data) {
    id = uuidv4();
  }
  let node = {
    children: [],
    id: id,
    name: name,
    parents: [],
    spouse: "",
  };
  data[id] = node;
  return node;
}
