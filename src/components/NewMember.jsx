import {
  Button,
  Grid,
  Paper,
  Snackbar,
  TextField,
  Typography,
} from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { createMember, createNode, getData } from "../utils/familyTreeUtils";
import { styled } from "@mui/material/styles";
import { AuthContext } from "../utils/context";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  color: theme.palette.text.secondary,
}));

class NewMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      allMemberInfo: {},
      loaded: false,
      createNewSnackbar: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleCreate = this.handleCreate.bind(this);
    this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);
  }

  async componentDidMount() {
    let data = await getData(this.context);
    this.setState({ allMemberInfo: data, loaded: true });
  }

  handleChange(event) {
    this.setState({
      data: event.target.value,
    });
  }

  async handleCreate() {
    let node = createMember(this.state.allMemberInfo, this.state.data);
    await createNode(node, this.context);
    const data = await getData(this.context);
    this.setState({
      allMemberInfo: data,
      createNewSnackbar: true,
    });
  }

  handleCloseSnackbar() {
    this.setState({
      createNewSnackbar: false,
    });
  }

  render() {
    if (!this.state.loaded) {
      return <div> Loading </div>;
    }
    return (
      <Box sx={{ width: "100%" }}>
        <Grid container spacing={2}>
          <Grid item xs={12} md={4}>
            <Item>
              <Typography variant="h6" component="h6">
                Member Details
              </Typography>
              <Snackbar
                open={this.state.createNewSnackbar}
                autoHideDuration={2000}
                message="Successfully created"
                onClose={this.handleCloseSnackbar}
              />
              <Box sx={{ width: "100%", my: 1 }}>
                <TextField
                  id="newMemberName"
                  label="Enter a name"
                  variant="outlined"
                  value={this.state.data}
                  onChange={this.handleChange}
                />
              </Box>
              <Box sx={{ width: "100%", my: 1 }}>
                <Button variant="outlined" onClick={this.handleCreate}>
                  Create
                </Button>
              </Box>
            </Item>
          </Grid>
          <Grid item xs={12} md={8}>
            <TextField
              variant="filled"
              id="outlined-textarea"
              multiline
              value={JSON.stringify(this.state.allMemberInfo, null, 2)}
              fullWidth
            />
          </Grid>
        </Grid>
      </Box>
    );
  }
}

NewMember.contextType = AuthContext;

export default NewMember;
