import { getAuth, onAuthStateChanged } from "@firebase/auth";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  Grid,
  Snackbar,
  Typography,
} from "@mui/material";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import React from "react";
import {
  getChildrenIds,
  getData,
  getMembers,
  getName,
  getRandomKeyValue,
  getRelationships,
  saveData,
  getPersonInfo,
} from "../utils/familyTreeUtils";
import { Node, Tree } from "../utils/treeUtils";
import FamilyTreeView from "./FamilyTree";
import { Members } from "./Members";
import { AuthContext } from "../utils/context";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

class RelationCard extends React.Component {
  render() {
    return (
      <Card variant="outlined">
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {this.props.value}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {this.props.type}
          </Typography>
        </CardContent>
        <CardActions>
          <Button
            size="small"
            onClick={() => this.props.onClick(this.props.id)}
          >
            View
          </Button>
          <Button
            size="small"
            onClick={() => this.props.onInfoClick(this.props.id)}
          >
            Info
          </Button>
        </CardActions>
      </Card>
    );
  }
}

class Relationship extends React.Component {
  render() {
    return (
      <Grid item xs={12} sm={6} md={6} lg={4}>
        <Item>
          <RelationCard
            id={this.props.id}
            type={this.props.type}
            value={this.props.value}
            onClick={this.props.onClick}
            onInfoClick={this.props.onInfoClick}
          />
        </Item>
      </Grid>
    );
  }
}

class Body extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.createTree = this.createTree.bind(this);
    this.handleRelationClick = this.handleRelationClick.bind(this);
    this.handleInfoClick = this.handleInfoClick.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.handleCloseSnackbar = this.handleCloseSnackbar.bind(this);

    this.state = {
      id: "",
      name: "",
      tree: "",
      inputId: "",
      selectedId: "",
      familyMembers: "",
      loaded: false,
      data: {},
      savedDataSnackbar: false,
    };
  }

  async componentDidMount() {
    const response = await getData(this.context);
    var [id, name] = getRandomKeyValue(response);
    if (id === "" && name === "") {
      window.location = "/create";
    } else {
      this.setState({
        id: id,
        name: name,
        tree: this.createTree(response, id, name),
        inputId: id,
        selectedId: id,
        familyMembers: getMembers(response),
        loaded: true,
        data: response,
      });
    }
  }

  createTree(data, id, name) {
    const root = new Node(id, name);
    return new Tree(data, root, getChildrenIds(data, root.getId()));
  }

  handleChange(id) {
    this.setState({
      inputId: id,
    });
  }

  handleClick() {
    const name = getName(this.state.data, this.state.inputId);
    const t = this.createTree(this.state.data, this.state.inputId, name);

    this.setState({
      id: this.state.inputId,
      name: name,
      tree: t,
    });
  }

  async handleRefresh() {
    let data = await getData(this.context);
    this.setState({
      data: data,
      familyMembers: getMembers(data),
    });
  }

  handleCloseSnackbar() {
    this.setState({
      savedDataSnackbar: false,
    });
  }

  async handleSave() {
    await saveData(this.context);
    this.setState({
      savedDataSnackbar: true,
    });
  }

  handleSelected(id) {
    this.setState({
      name: getName(this.state.data, id),
      selectedId: id,
    });
  }

  handleRelationClick(id) {
    const name = getName(this.state.data, id);
    const t = this.createTree(this.state.data, id, name);
    this.setState({
      id: id,
      name: name,
      tree: t,
      inputId: id,
      selectedId: id,
    });
  }

  async handleInfoClick(id) {
    const personInfo = await getPersonInfo(id, this.context);
    alert(JSON.stringify(personInfo));
  }

  getRelations() {
    if (this.state.loaded) {
      var relationships = getRelationships(
        this.state.data,
        this.state.selectedId
      ).map((x) => {
        return (
          <Relationship
            id={x["id"]}
            type={x["key"]}
            value={x["value"]}
            onClick={this.handleRelationClick}
            onInfoClick={this.handleInfoClick}
          />
        );
      });
      return relationships;
    }
  }

  render() {
    if (!this.state.loaded) {
      return <div> Loading </div>;
    }

    const auth = getAuth(this.context);
    onAuthStateChanged(auth, (user) => {
      if (user) {
        // stay
      } else {
        this.props.logoutHandler();
      }
    });

    return (
      <Grid container spacing={2} sx={{ my: 10 }}>
        <Grid container spacing={2}>
          <Grid
            item
            md={12}
            sx={{
              display: { xs: "none", md: "block" },
            }}
          >
            <Card variant="outlined">
              <CardContent>
                <Box component="div">
                  <Members
                    familyMembers={this.state.familyMembers}
                    onChange={(selection) => {
                      if (selection === undefined || selection === null) {
                        return;
                      }
                      this.handleChange(selection["id"], "id");
                    }}
                  />
                </Box>

                <Box component="div" mt={1}>
                  <Button
                    variant="contained"
                    onClick={() => this.handleClick()}
                    sx={{ mr: 2 }}
                  >
                    Update
                  </Button>
                  <Button
                    variant="contained"
                    onClick={() => this.handleRefresh()}
                    sx={{ mr: 2 }}
                  >
                    Refresh
                  </Button>
                  <Button variant="contained" onClick={() => this.handleSave()}>
                    Save
                  </Button>

                  <Snackbar
                    open={this.state.savedDataSnackbar}
                    autoHideDuration={2000}
                    message="Successfully saved"
                    onClose={this.handleCloseSnackbar}
                  />
                </Box>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid
            item
            md={5}
            lg={4}
            sx={{
              display: { xs: "none", md: "block" },
            }}
          >
            <Box sx={{ flexGrow: 1 }}>
              <FamilyTree
                data={this.state.data}
                rootId={this.state.id.toString()}
                name={getName(this.state.data, this.state.id.toString())}
                tree={this.state.tree}
                onSelect={(selectedId) => this.handleSelected(selectedId)}
              />
            </Box>
          </Grid>
          <Grid item lg={8} md={7}>
            <Box sx={{ flexGrow: 1 }}>
              <Card
                variant="outlined"
                sx={{
                  my: 1,
                  bgcolor: "info.main",
                  color: "info.contrastText",
                }}
              >
                <CardContent>
                  <Typography display="inline" variant="body" sx={{ mr: 2 }}>
                    Currently Viewing
                  </Typography>
                  <Typography display="inline" variant="h5">
                    {this.state.name}
                  </Typography>
                </CardContent>
              </Card>

              <Grid container spacing={2}>
                {this.getRelations(this.state.selectedId)}
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

function FamilyTree(props) {
  const rootName = getName(props.data, props.rootId);
  return (
    <Box>
      <Card
        variant="outlined"
        sx={{ my: 1, bgcolor: "info.main", color: "info.contrastText" }}
      >
        <CardContent>
          <Typography variant="h5">Family tree for {rootName}</Typography>
        </CardContent>
      </Card>
      <Card variant="outlined">
        <CardContent>
          <FamilyTreeView
            data={props.data}
            rootId={props.rootId}
            name={rootName}
            tree={props.tree}
            onSelect={props.onSelect}
          />
        </CardContent>
      </Card>
    </Box>
  );
}

Body.contextType = AuthContext;
export default Body;
