import React from "react";

import {
  getAuth,
  signInWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
} from "firebase/auth";

import {
  Container,
  Box,
  TextField,
  Button,
  Grid,
  Typography,
} from "@mui/material";

import { AuthContext } from "../utils/context";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      loggedOut: true,
      user: undefined,
    };
    this.setUserName = this.setUserName.bind(this);
    this.setPassword = this.setPassword.bind(this);
  }

  async loginHelper() {
    const auth = getAuth(this.context);
    await signInWithEmailAndPassword(
      auth,
      this.state.username,
      this.state.password
    );
    this.setState({
      loggedOut: false,
    });
    this.props.successHandler();
  }

  async handleLogout() {
    const auth = getAuth(this.context);
    await signOut(auth);
    this.setState({
      username: "",
      password: "",
      loggedOut: true,
    });

    this.props.logoutHandler();
  }

  handleLogin() {
    if (this.state.loggedOut === true) {
      this.loginHelper();
    }
  }

  setUserName(e) {
    this.setState({
      username: e.target.value,
    });
  }

  setPassword(e) {
    this.setState({
      password: e.target.value,
    });
  }

  componentDidMount() {
    const auth = getAuth(this.context);

    onAuthStateChanged(auth, (user) => {
      if (user) {
        this.setState({
          loggedOut: false,
          username: user.email,
          user: user.email,
        });
        this.props.successHandler();
      } else {
        this.setState({ loggedOut: true, username: "", password: "" });
      }
    });
  }

  render() {
    if (!this.state.user) {
      return (
        <Container maxWidth="sm">
          <Box
            sx={{
              my: 20,
            }}
          />
          <Typography variant="h5" gutterBottom component="div">
            Login to FamilyTreeMaker
          </Typography>

          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="username"
                label="Username"
                variant="outlined"
                value={this.state.username}
                onChange={this.setUserName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="password"
                label="Password"
                variant="outlined"
                type="password"
                value={this.state.password}
                onChange={this.setPassword}
              />
            </Grid>
            <Grid item xs={12}>
              <Button
                id="login"
                variant="outlined"
                onClick={() => this.handleLogin()}
              >
                Login
              </Button>
            </Grid>
          </Grid>
        </Container>
      );
    } else {
      return <div> Hi </div>;
    }
  }
}

class Login extends React.Component {
  render() {
    return (
      <div>
        <LoginPage
          successHandler={this.props.successHandler}
          logoutHandler={this.props.logoutHandler}
        />
      </div>
    );
  }
}

LoginPage.contextType = AuthContext;

export default Login;
