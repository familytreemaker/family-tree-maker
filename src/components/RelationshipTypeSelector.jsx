import { Autocomplete, TextField } from "@mui/material";

export function RelationshipTypeSelector(props) {
  let relationshipTypes = [
    { id: 0, label: "parent of" },
    { id: 1, label: "spouse of" },
    { id: 2, label: "child of" },
  ];
  return (
    <Autocomplete
      disablePortal
      id="familyMembers"
      options={relationshipTypes}
      sx={{ maxWidth: 300 }}
      renderInput={(params) => (
        <TextField {...params} label="Select relationship type" />
      )}
      defaultValue={relationshipTypes[0]}
      onChange={(event, selection) => {
        props.onChange(selection);
      }}
    />
  );
}
