import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Header from "./Header";
import Body from "./Body";
import Footer from "./Footer";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import FamilyTreeCreator from "./FamilyTreeCreator";
import Login from "./Login";
import app from "../config/firebaseConfig";
import Logout from "./Logout";
import { getAuth, onAuthStateChanged } from "@firebase/auth";
import { Backdrop, CircularProgress, Typography } from "@mui/material";
import { AuthContext } from "../utils/context";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loginState: false,
      initializing: false,
    };
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
  }

  login() {
    this.setState({
      loginState: true,
    });
  }

  logout() {
    this.setState({
      loginState: false,
      initializing: false,
    });
  }

  componentDidMount() {
    if (!this.state.loginState && !this.state.initializing) {
      const auth = getAuth(app);
      this.setState({
        initializing: true,
      });
      onAuthStateChanged(auth, (user) => {
        if (user) {
          this.setState({ loginState: true, initializing: false });
        } else {
          this.setState({ loginState: false, initializing: false });
        }
      });
    }
  }

  render() {
    if (!this.state.loginState && !this.state.initializing) {
      return (
        <React.Fragment>
          <CssBaseline />
          <AuthContext.Provider value={app}>
            <Container>
              <Header />
              <Login successHandler={this.login} logoutHandler={this.logout} />
              <Footer />
            </Container>
          </AuthContext.Provider>
        </React.Fragment>
      );
    } else if (!this.state.loginState && this.state.initializing) {
      return (
        <React.Fragment>
          <CssBaseline />
          <AuthContext.Provider value={app}>
            <Container>
              <Header />
              <Typography
                variant="h5"
                gutterBottom
                component="div"
                sx={{ my: 15 }}
              >
                <Backdrop
                  sx={{
                    color: "#fff",
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                  }}
                  open={this.state.initializing}
                >
                  <CircularProgress color="inherit" />
                </Backdrop>
              </Typography>
              <Footer />
            </Container>
          </AuthContext.Provider>
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment>
          <CssBaseline />
          <AuthContext.Provider value={app}>
            <Container>
              <Header />
              <Router>
                <Switch>
                  <Route path="/view">
                    <Body logoutHandler={this.logout} />
                  </Route>
                  <Route path="/create">
                    <FamilyTreeCreator logoutHandler={this.logout} />
                  </Route>
                  <Route path="/logout">
                    <Logout logoutHandler={this.logout} />
                  </Route>
                  <Route path="/">
                    <Body logoutHandler={this.logout} />
                  </Route>
                </Switch>
              </Router>
              <Footer />
            </Container>
          </AuthContext.Provider>
        </React.Fragment>
      );
    }
  }
}

export default App;
