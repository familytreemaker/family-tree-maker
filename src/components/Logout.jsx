import * as React from "react";

import { getAuth, signOut } from "firebase/auth";
import { AuthContext } from "../utils/context";

class Logout extends React.Component {
  render() {
    const auth = getAuth(this.context);
    signOut(auth)
      .then(() => {
        this.props.logoutHandler();
        window.location = "/";
      })
      .catch((error) => {});
    return <div>Logging out ...</div>;
  }
}

Logout.contextType = AuthContext;
export default Logout;
