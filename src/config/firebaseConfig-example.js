// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "xx",
  authDomain: "xx",
  projectId: "xx",
  storageBucket: "xx",
  messagingSenderId: "xx",
  appId: "xx",
  measurementId: "xx",
};

const app = initializeApp(firebaseConfig);

export default app;
