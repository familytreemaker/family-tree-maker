# Important Documentation


## dfs_helper in treeUtils.js

This really is the heart of our project.

```
parentFunctionNoChildren: callback function for a node without children
    params: node -> Object of the Node class currently visited by DFS
    returns: for the purposes of this project, it returns a tag
        see handleNoChild in FamilyTree.jsx
parentFunctionWithChildren: callback function for a node with children
    params: node, children, path
        node -> Object of the Node class currently visited by DFS
        children -> Array of Node objects that correspond to the current Node's children
        path -> path of nodes getting visited.
    returns: for the purposes of this project, it returns a TreeItem with children
        see handleChilds in FamilyTree.jsx
See docs on DFS:
    https://en.wikipedia.org/wiki/Depth-first_search#Pseudocode

Purpose of path:
    Consider this hierarchy:
        grandmother -> mother -> daughter
    As you visit the grandmother node, you'll try to add mother as the child.
    However, the mother node also has a child - the daughter node.
    So, you'll need to first add daughter as mother's child,
    then add mother to grandmother, so that the grandmother node
    will have mother, which then contains the child.
    
    Path makes sure that it visits the children, then invokes the parent function
```