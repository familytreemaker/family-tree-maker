import json

class Person:
    def __init__(self, line):
        self.line = line
        self.properties = {}
        if self.is_valid_line(line):
            self.parseLine()
        else:
            raise ValueError("Invalid line")
    
    def getTokens(self):
        tokens = self.line.split('\t')
        tokens = list(filter(lambda x: x != '', tokens))
        tokens = list(map(lambda x: x.replace('\n', '').strip(), tokens))
        tokens = list(map(lambda x: x.capitalize(), tokens))
        return tokens


    def parseLine(self):
        tokens = self.getTokens()
        self.properties["id"] = self.parseID(tokens)
        self.properties["name"] = self.parseName(tokens)
        self.properties["spouse"] = self.parseSpouse(tokens)


    def startsWithValidLetter(self, line):
        return line[0] == 'A'


    def is_valid_line(self, line):
        return self.startsWithValidLetter(line)
    

    def parseID(self, tokens):
        tokens = list(filter(lambda x: len(x) == 1, tokens))
        return '-'.join(tokens)
    

    def stripID(self, tokens):
        tokens_without_id_prefix = tokens[1:]
        id = list(filter(lambda x: x != 'A', tokens_without_id_prefix))
        id = list(filter(lambda x: not (x >= '0' and x <= '9'), id))
        return id
    

    def parseName(self, tokens):
        tokens = self.stripID(tokens)
        return tokens[1] if tokens[0].lower() in ['son', 'daughter'] else tokens[0]
    
    def stripName(self, tokens):
        tokens = self.stripID(tokens)
        tokens = tokens[1:] if tokens[0].lower() in ['son', 'daughter'] else tokens[0:]
        if len(tokens) > 1:
            return tokens[1:]
        else:
            return []
    
    def parseSpouse(self, tokens):
        tokens = self.stripName(tokens)
        return tokens[0] if len(tokens) == 1 else ""

    def getSpouse(self):
        return self.properties["spouse"]
    
    def getSpouseID(self):        
        return self.getID().replace('A', 'B') if 'A' in self.getID() else self.getID().replace('B', 'A')
    
    def __str__(self):
        return self.properties["name"]
    
    def getID(self):
        return self.properties["id"]
    
    def getName(self):
        return self.properties["name"]


class Tree:
    class PersonObject:
        def __init__(self, personInfo):
            self.name = personInfo.get("name")
            self.spouse = personInfo.get("spouse")
            self.children = personInfo.get("children")
            self.parents = personInfo.get("parents")
            # self.childrenObjects = []
            self.id = personInfo.get("id")
        
        def getName(self):
            return self.name
        
        def getChildren(self):
            return self.children

        def getID(self):
            return self.id
        
        def getSpouse(self):
            return self.spouse
        
        def addChild(self, child):
            self.children.append(child.getID())
            # self.childrenObjects.append(child)
        
        def addParent(self, parent):
            self.parents.append(parent.getID())
            self.parents.append(parent.getSpouse())

        def getAll(self):
            details = {
                "name": self.name,
                "spouse": self.spouse,
                "children": self.children,
                "parents": self.parents,
                "id": self.id
            }
            return details
        
        def __str__(self):
            return self.getAll()

    def __init__(self, persons):
        self.data = {}
        for person in persons:
            self.add(person)
        self.populateChildren()

    def add(self, person: Person):
        id = person.getID()
        personInfo = {
            "name": person.getName(),
            "spouse": person.getSpouseID(),
            "children": [],
            "parents": [],
            "id": id
        }
        self.data[id] = self.PersonObject(personInfo)
        if person.getSpouseID() not in self.data:
            spouseInfo = {
                "name": person.getSpouse(),
                "spouse": person.getID(),
                "children": [],
                "parents": [],
                "id": person.getSpouseID()
            }
            self.data[person.getSpouseID()] = self.PersonObject(spouseInfo)

    def getAll(self):
        return self.data
    
    def populateChildren(self):
        for person in self.data:
            currentPerson: self.PersonObject = self.data.get(person)
            currentPersonIDLevels = len(currentPerson.getID().split("-"))

            if 'B' in currentPerson.getID():
                continue

            for people in self.data:
                relatedPerson: self.PersonObject = self.data[people]
                relatedPersonIDLevels = len(relatedPerson.getID().split("-"))
                if (len(currentPerson.getID()) < len(relatedPerson.getID())) and \
                    currentPerson.getID() in relatedPerson.getID() and \
                    (relatedPersonIDLevels - currentPersonIDLevels == 1):
                    self.data[person].addChild(relatedPerson)
                    self.data[relatedPerson.getID()].addParent(currentPerson)
                    spouseID = currentPerson.getID().replace('A', 'B')
                    spouse = self.data.get(spouseID)
                    spouse.addChild(relatedPerson)


def parseDataFile(data_lines):
    results = []    
    for line in data_lines:
        try:
            person = Person(line)
            results.append(person)
        except ValueError:
            pass
    t = Tree(results)
    return t


def parse(file):
    with open(file) as data_file:
        lines = []
        for line in data_file:
            lines.append(line)
    return parseDataFile(lines)


if __name__ == "__main__":
    tree = parse("./samplefile.txt")
    print(json.dumps(tree.getAll(), default=lambda o: o.__dict__, sort_keys=True, indent=2))
